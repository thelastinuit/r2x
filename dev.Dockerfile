FROM ruby:2.4

RUN mkdir -p /code
ADD . /code
WORKDIR /code

RUN gem install bundler --no-ri --no-rdoc

RUN bundle
