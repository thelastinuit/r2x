<p align="center">
  <img src="r2x.png" alt="r2x">
</p>

# r2x (/ɑːrˈskwerɛks/)
An opinionated redux-like state container for ruby apps.

## Setup
Add to your `Gemfile`

```ruby
gem 'r2x'
```

## Usage
Define actions and reducers as such:
```ruby
state = {
  tasks: []
}

R2x::Store.build(state) do |s|
  actions do
    tasks :add_task, :remove_stask
    task :toggle
  end

  reducers do
    tasks do |state, action|
      case action[:type]
      when :add_task
        state[:tasks] << action[:value]
      else
        state
      end
    end

    task do |action|
      # And so on..
    end
  end
end
```

## Development
If you are into `docker`, you just need to:
```bash
$ docker-compose build
```

after you build the image, you can get into `test` service:
```bash
$ docker-compose run --rm test bash
```

And run the current specs `rspec`. 
