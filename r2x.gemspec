# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'r2x/version'

Gem::Specification.new do |spec|
  spec.name          = 'r2x'
  spec.version       = R2x::VERSION
  spec.authors       = ['Mr. Eyebrows']
  spec.email         = ['email@luisignac.io']

  spec.summary       = 'An opinionated redux-like state container for ruby apps.'
  spec.description   = 'A very, very opinionated redux-like state container for ruby apps.'
  spec.homepage      = 'https://github.com/thelastinuit/r2x'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
